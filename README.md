# QGIS3x_Template_GBA

Dieses Repository enthält alle benötigten Files um in QGIS 3.x nach Standards der Geologischen Bundesanstalt Österreichs (GBA) eine digitale geologische Karte zu erstellen. Für genauere Informationen bzw. Anleitungen öffnen sie bitte die jeweiligen PDFs in diesem Repository.<br><br>

Eine genaue Beschreibung findet sich im [Wiki des Projekts](https://gitea.geologie.ac.at/geolba/QGIS3x_Template_GBA/wiki) <br><br>

Projektarbeit zur [standartisierten geologischen Datenverarbeitung mit QGIS 3](https://gitea.geologie.ac.at/geolba/QGIS3x_Template_GBA/src/branch/main/Projektarbeit_u105575_Standardisierte%20geologische%20Datenverwaltung%20mit%20QGIS3_x_V_1.0.pdf)

