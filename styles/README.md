
GBA TrueTypeFonts<br>
Download GBA True Type Fonts<br> https://gisgba.geologie.ac.at/LegendGenerator/Files/TrueTypeFonts.zip

GBA Stylefiles<br>
Download GBA Stylefiles<br> https://gisgba.geologie.ac.at/LegendGenerator/Files/Stylefiles.zip

Sylefile Descriptions<br>
Download PDF Descriptions<br> https://gisgba.geologie.ac.at/LegendGenerator/Files/StyleDescriptions.zip